#include "Initializable.h"

Initializable::~Initializable() {
}


void Initializable::init() {
    if(inited)
        return;
    inited = true;
    onInitialize();
}