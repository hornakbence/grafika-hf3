#include <cstdio>
#include <cstdlib>

#include "glsl_check.h"

#include "Shader.h"

Shader::Shader(GLenum type, const char* source)
: type(type), source(source) {
}

void Shader::onInitialize() {
    
    _ref = glCreateShader(type);
    if (!_ref) {
        printf("Error in vertex shader creation\n");
        exit(1);
    }
    glShaderSource(_ref, 1, &source, NULL);
    glCompileShader(_ref);
    checkShader(_ref, "Shader error");
}

GLuint Shader::ref() const {
    return _ref;
}

void Shader::draw() {
    onDraw();
}


void Shader::onDraw() {
}

