#ifndef GLSL_CHECK_H
#define GLSL_CHECK_H

#include <GL/glew.h>

void getErrorInfo(unsigned int handle);

// check if shader could be compiled

void checkShader(unsigned int shader, const char * message);

// check if shader could be linked

void checkLinking(unsigned int program);

#endif /* GLSL_CHECK_H */

