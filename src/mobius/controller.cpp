#include <cstdio>

#include "includes.h"
#include "config.h"
#include "controller.h"
#include "MobiusWorld.h"
#include "MobiusProgram.h"

MobiusProgram program;

void onInitialization() {
    glViewport(0, 0, windowWidth, windowHeight);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    program.init();
    //    world.init();
}

void onExit() {
    printf("exit\n");
}

// Window has become invalid: Redraw

void onDisplay() {
    glClearColor(0, 0, 0, 0); // background color 
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the screen

    program.draw();

    glutSwapBuffers(); // exchange the two buffers
}

// Key of ASCII code pressed

void onKeyboard(unsigned char key, int pX, int pY) {
    switch (key) {
        case ' ':
            program.toggleView();
            break;
        case 'w':
            program.stepLeft();
            break;
        case 'p':
            program.stepRight();
            break;
    }
}

// Key of ASCII code released

void onKeyboardUp(unsigned char key, int pX, int pY) {
}

// Mouse click event

void onMouse(int button, int state, int pX, int pY) {
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) { // GLUT_LEFT_BUTTON / GLUT_RIGHT_BUTTON and GLUT_DOWN / GLUT_UP
        float cX = 2.0f * pX / windowWidth - 1; // flip y axis
        float cY = 1.0f - 2.0f * pY / windowHeight;

        (void) cX;
        (void) cY;

        glutPostRedisplay(); // redraw
    }
}

// Move mouse with key pressed

void onMouseMotion(int pX, int pY) {
}

// Idle event indicating that some time elapsed: do animation here

void onIdle() {
    static float tend = 0;
    const float dt = 0.1; // dt is infinitesimal
    float tstart = tend;
    tend = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;

    for (float t = tstart; t < tend; t += dt) {
        float Dt = fmin(dt, tend - t);
        program.onAnimate(t, Dt);
    }
    glutPostRedisplay();
}
