#ifndef ROLLINGTORUSDRAWER_H
#define ROLLINGTORUSDRAWER_H

#include "Drawer.h"

class MobiusWorld;
class RollingTorus;

class RollingTorusDrawer : public Drawer {
    const RollingTorus& rollingTorus;
public:
    RollingTorusDrawer(MobiusWorld& world, const Material& material,
            const RollingTorus& rollingTorus);
    void getTransformM(mat4& m, mat4& mInv) override;

};

#endif /* ROLLINGTORUSDRAWER_H */

