#ifndef INCLUDES_H
#define INCLUDES_H

#define _USE_MATH_DEFINES  // Van M_PI
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>

#if defined(__APPLE__)
#include <GLUT/GLUT.h>
#include <OpenGL/gl3.h>
#include <OpenGL/glu.h>
#else
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#include <windows.h>
#endif
#include <GL/glew.h>  // must be downloaded 
#include <GL/freeglut.h> // must be downloaded unless you have an Apple
#endif

#endif /* INCLUDES_H */

