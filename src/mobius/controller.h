#ifndef CALLBACKS_H
#define CALLBACKS_H

void onInitialization();

void onExit();

// Window has become invalid: Redraw

void onDisplay();

// Key of ASCII code pressed

void onKeyboard(unsigned char key, int pX, int pY);

// Key of ASCII code released

void onKeyboardUp(unsigned char key, int pX, int pY);

// Mouse click event

void onMouse(int button, int state, int pX, int pY);

// Move mouse with key pressed

void onMouseMotion(int pX, int pY);

// Idle event indicating that some time elapsed: do animation here

void onIdle();

#endif /* CALLBACKS_H */

