#ifndef SKYTEXTURE_H
#define SKYTEXTURE_H

#include "model/Texture.h"

class SkyTexture : public Texture {
    int width, height;
    float starProbability;
public:
    SkyTexture(ShaderProgram& program, const char* samplerName,
            GLuint textureUnit, int width, int height, float starProbability);
    void onUploadTexture() override;


};

#endif /* SKYTEXTURE_H */

