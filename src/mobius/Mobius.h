#ifndef MOBIUS_H
#define MOBIUS_H

#include "model/ParamSurface.h"
#include "model/Camera.h"

class Mobius : public ParamSurface {
    GLfloat radius, width;
public:
    Mobius(Texture* texture, ParamSurfaceDrawer& drawer, GLfloat radius, GLfloat width);
    VertexData generateVertexData(GLfloat u, GLfloat v) override;

    void setCamera(Camera& camera, GLfloat u, GLfloat v, GLfloat height) const;
    void getRollingPosition(mat4& m, mat4& mInv, GLfloat u, GLfloat v,
            GLfloat radius) const;

};

#endif /* MOBIUS_H */

