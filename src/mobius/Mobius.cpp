#include <cmath>

#include "Mobius.h"
#include "model/vec4.h"
#include "model/mat4.h"

Mobius::Mobius(Texture* texture, ParamSurfaceDrawer& drawer, GLfloat radius, GLfloat width) :
ParamSurface(texture, drawer, 5, 300, 5, 17), radius(radius), width(width) {
}

VertexData Mobius::generateVertexData(GLfloat u, GLfloat v) { // v rolls over ONCE!
    vec3 base = vec4(0, 1, 0) * radius * mat4::rotateX(2 * M_PI * v);

    vec3 side = vec4(1, 0, 0) * width * mat4::rotateZ(M_PI * v)
            * mat4::rotateX(2 * M_PI * v) * (u - .5);

    vec3 vec = base + side;

    vec2 texCoords(u, v);
    return VertexData(vec, vec, texCoords);
}

void Mobius::setCamera(Camera& camera, GLfloat u, GLfloat v, GLfloat height) const { // v rolls over TWICE!
    vec3 base = vec4(0, 1, 0) * radius * mat4::rotateX(4 * M_PI * v);

    mat4 transform = mat4::rotateZ(2 * M_PI * v)
            * mat4::rotateX(4 * M_PI * v);

    vec3 side = vec4(1, 0, 0) * width * (u - .5) * transform;
    vec3 upNorm = vec4(0, 1, 0) * transform;
    vec3 up = upNorm * height;
    vec3 forward = vec4(0, 0, -1) * transform;

    camera.wEye = base + side + up;
    camera.wVup = upNorm;
    camera.wLookat = (base + side + up) + forward;

}

void Mobius::getRollingPosition(mat4& m, mat4& mInv, GLfloat u, GLfloat v, GLfloat r) const { // v rolls over TWICE!
    vec3 base = vec4(0, 1, 0) * radius * mat4::rotateX(4 * M_PI * v);

    mat4 rotateUp = mat4::rotateZ(2 * M_PI * v) * mat4::rotateX(4 * M_PI * v);
    mat4 rotateUpInv = mat4::rotateX(-4 * M_PI * v) * mat4::rotateZ(-2 * M_PI * v);

    vec3 side = vec4(1, 0, 0) * width * (u - .5) * rotateUp;
    vec3 upNorm = vec4(0, 1, 0) * rotateUp;
    vec3 up = upNorm * r;

    vec3 pos = base + side + up;

    mat4 rotatePhase = mat4::rotateX(radius / r * v + 4 * M_PI * v);
    mat4 rotatePhaseInv = mat4::rotateX(-radius / r * v + 4 * M_PI * v);

    mat4 translate = mat4::translate(pos);
    mat4 translateInv = mat4::translate(-pos);

    m = rotatePhase * rotateUp * translate;
    mInv = translateInv * rotateUpInv * rotatePhaseInv;
}
