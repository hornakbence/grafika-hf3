#include "RollingTorusDrawer.h"
#include "RollingTorus.h"

RollingTorusDrawer::RollingTorusDrawer(MobiusWorld& world,
        const Material& material, const RollingTorus& rollingTorus)
: Drawer(world, material), rollingTorus(rollingTorus) {
}

void RollingTorusDrawer::getTransformM(mat4& m, mat4& mInv) {
    rollingTorus.getTransformM(m, mInv);
}
