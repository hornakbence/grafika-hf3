#ifndef LIGHT_H
#define LIGHT_H

#include "model/vec3.h"
#include "model/vec4.h"

class Light {
public:
    const vec3 La, Le;
    const vec4 wLightPos;

    Light(const vec3 La, const vec3 Le, const vec4 wLightPos);
};

#endif /* LIGHT_H */

