#include "Torus.h"
#include <cmath>
#include "mat4.h"
#include "vec4.h"

Torus::Torus(Texture* texture, ParamSurfaceDrawer& drawer, GLfloat baseRadius,
        GLfloat innerRadius)
: ParamSurface(texture, drawer, 50, 100), baseRadius(baseRadius),
innerRadius(innerRadius) {
}

VertexData Torus::generateVertexData(GLfloat u, GLfloat v) {
    vec3 base = vec4(0, 1, 0) * mat4::rotateX(2 * M_PI * u) * baseRadius;
    vec3 plusNormal = vec4(0, 1, 0) * mat4::rotateZ(2 * M_PI * v) * mat4::rotateX(2 * M_PI * u);
    vec3 plus = plusNormal * innerRadius;

    vec3 vec = base + plus;
    vec2 texCoords(u, v);
    return VertexData(vec, plus, texCoords);
}
