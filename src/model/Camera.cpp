#include "Camera.h"
#include <cmath>

Camera::Camera(GLfloat fov, GLfloat asp,
        GLfloat fp, GLfloat bp) :
fov(fov), asp(asp), fp(fp), bp(bp) {
}

mat4 Camera::transformP() const {
    return mat4(1 / (tan(fov / 2) * asp), 0, 0, 0,
            0, 1 / tan(fov / 2), 0, 0,
            0, 0, -(fp + bp) / (bp - fp), -1,
            0, 0, -2 * fp * bp / (bp - fp), 0);
}

mat4 Camera::transformV() const {
    vec3 w = (wEye - wLookat).normalize();
    vec3 u = wVup.cross(w).normalize();
    vec3 v = w.cross(u);
    return mat4::translate(-wEye)
            * mat4(u.x, v.x, w.x, 0,
            u.y, v.y, w.y, 0,
            u.z, v.z, w.z, 0,
            0, 0, 0, 1);
}
