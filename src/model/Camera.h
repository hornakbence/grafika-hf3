#ifndef CAMERA_H
#define CAMERA_H

#include "GL/glew.h"
#include "vec3.h"
#include "mat4.h"

class Camera {
public:
    vec3 wEye, wLookat, wVup; // extinsic
    const GLfloat fov, asp, fp, bp; // intrinsic

    Camera(GLfloat fov, GLfloat asp, GLfloat fp, GLfloat bp);

    mat4 transformV() const;
    mat4 transformP() const;
};

#endif /* CAMERA_H */

