#include "ParamSurface.h"

VertexData::VertexData() {
}

VertexData::VertexData(vec3& position, vec3& normal, vec2& texcoord) :
position(position), normal(normal.normalize()), texcoord(texcoord) {
}

ParamSurface::ParamSurface(Texture* texture, ParamSurfaceDrawer& drawer, 
        int n, int m, int holeSize, int period)
: Drawable(texture), drawer(drawer), n(n), m(m), holeSize(holeSize), period(period) {
}

ParamSurface::~ParamSurface() {
}

void ParamSurface::onInitialize() {
    Drawable::onInitialize();
    drawer.init();
    createTriangles();
}

void ParamSurface::onDraw() {
    drawer.drawTriangles(numVertices);
}

void ParamSurface::createTriangles() {
    // n: width, m: height
    VertexData* vertices = new VertexData[m * n];
    std::vector<VertexData> triangleGrid;

    generateAllVertexData(n, m, vertices);
    generateTriangleGrid(n, m, triangleGrid, vertices);
    numVertices = triangleGrid.size();
    drawer.uploadTriangles(numVertices, triangleGrid.data());

    delete[] vertices;
}

void ParamSurface::generateAllVertexData(int n, int m, VertexData* dest) {
    for (int j = 0; j < m; ++j)
        for (int i = 0; i < n; ++i)
            dest[j * n + i] = generateVertexData(i / (n - 1.0),
                j / (m - 1.0));
}

void ParamSurface::generateTriangleGrid(int n, int m,
        std::vector<VertexData>& dest, VertexData* vertices) {
    int countInPeriod = 0;
    for (int i = 0; i < n - 1; ++i) {
        for (int j = 0; j < m - 1; ++j) {
            if (countInPeriod >= holeSize) {
                dest.push_back(vertices[j * n + i]);
                dest.push_back(vertices[(j + 1) * n + i]);
                dest.push_back(vertices[j * n + (i + 1)]);

                dest.push_back(vertices[j * n + (i + 1)]);
                dest.push_back(vertices[(j + 1) * n + i]);
                dest.push_back(vertices[(j + 1) * n + (i + 1)]);
            }
            if (++countInPeriod == period)
                countInPeriod = 0;
        }
    }
}

ParamSurfaceDrawer::~ParamSurfaceDrawer() {
}
