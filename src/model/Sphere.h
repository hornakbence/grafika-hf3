#ifndef SPHERE_H
#define SPHERE_H

#include "ParamSurface.h"

class Sphere : public ParamSurface {
    GLfloat radius;
public:
    Sphere(Texture* texture, ParamSurfaceDrawer& drawer, GLfloat radius);
    
    VertexData generateVertexData(GLfloat u, GLfloat v) override;

};

#endif /* SPHERE_H */

