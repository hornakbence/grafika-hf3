#include "Sphere.h"
#include "model/ParamSurface.h"
#include "model/vec4.h"
#include "model/mat4.h"
#include <cmath>

Sphere::Sphere(Texture * texture, ParamSurfaceDrawer& drawer, GLfloat radius)
: ParamSurface(texture, drawer, 100, 100), radius(radius) {
}

VertexData Sphere::generateVertexData(GLfloat u, GLfloat v) {
    vec3 vec = vec4(1, 0, 0) * mat4::rotateZ(M_PI * (u - .5))
            * mat4::rotateY(2 * M_PI * v) * radius;

    vec2 texCoords(u, v);
    return VertexData(vec, vec, texCoords);
}
