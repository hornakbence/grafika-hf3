#ifndef WORLD_HPP
#define WORLD_HPP

#include <vector>

#include "gl/Initializable.h"
#include "Drawable.h"
#include "Texture.h"

class ShaderProgram; // circular dependency

class World : public Drawable {
public:
    ShaderProgram& program;
protected:
    std::vector<Drawable*> objects;
public:
    World(ShaderProgram& program);
    virtual ~World();

    void onInitialize() override;
    void onDraw() override;

};

#endif /* WORLD_HPP */

