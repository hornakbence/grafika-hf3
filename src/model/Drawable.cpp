#include "Drawable.h"

Drawable::Drawable(Texture* texture) : texture(texture) {
}

Drawable::~Drawable() {
}

void Drawable::draw() {
    if(texture != nullptr)
        texture->bind();
    onDraw();
}

void Drawable::onDraw() {
}

void Drawable::onInitialize() {
    texture->init();
}
